**Data Analyst Nanodegree Projects**

*  Includes my Data Analyst for Enterprise Nanodegree Program offered by Udacity.

**How many projects are there?**

*  There are 5 projects in total for this nanodegree 

****
As many people from around the world I applied to this amazing scholarship offered by Bertelsmann in collaboration with Udacity where they selected 15,000 participants to be a part of a challenge course! And after 3 months 10% of the participants are lucky enough to be selected to move to Phase 2 of the challenge where they’re offered full scholarships in one of 3 Nanodegrees offered by Udacity. I made the selected 10%